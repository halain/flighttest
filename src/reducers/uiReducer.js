import { actionTypes } from "../types/actionTypes";


const initialState = {
    msgError: null
}


export const uiReducer = ( state = initialState, action) => {

    switch (action.type) {
        case actionTypes.uiSetError:
            return {
                ...state, 
                msgError: action.payload
            }
            
        case actionTypes.uiRemoveError:
            return {
                ...state, 
                msgError: null
            }
                   
    
        default:
            return state;
    }

}