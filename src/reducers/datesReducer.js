import { actionTypes } from "../types/actionTypes"

const initialState = {
    departure_day: '',
    return_day: ''
}


export const datesReducer = ( state = initialState, action ) => {
    switch (action.type) {

        case actionTypes.departure_day:
            return {
                ...state,
                departure_day: action.payload.departure_day,
            }

        case actionTypes.return_day:
            return {
                ...state,
                return_day: action.payload.return_day,
            }

        default:
            return state

    }

}
