import { actionTypes } from "../types/actionTypes";

const initialState ={
    url:''
}

export const urlReducer = ( state = initialState, action ) => {

    switch (action.type) {

        case actionTypes.url:
            return {
                url: action.payload
            }

        default:
            return state
    }

 }
