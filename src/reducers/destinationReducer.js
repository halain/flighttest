import { actionTypes } from "../types/actionTypes"


const initialState = {
    origin: '',
    destination: ''
}
 

export const destinationReducer = ( state = initialState, action ) => {
  
    switch (action.type) {

        case actionTypes.origin:
            return {
                ...state,
                origin: action.payload.origin,
            }

        case actionTypes.destination:
            return {
                ...state,
                destination: action.payload.destination,
            }

        default:
            return state

    }

}
