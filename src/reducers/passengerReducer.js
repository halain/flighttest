import { actionTypes } from "../types/actionTypes";

const initialState = {
    adults: 1, 
    infants: 0,
    childrens: 0
}


export const passengerReducer = ( state = initialState , action ) => {
  
    switch (action.type) {

        case actionTypes.adult_amount:
            return {
                ...state,
                adults: action.payload.adults,
            }

        case actionTypes.children_amount:
            return {
                ...state,
                childrens: action.payload.childrens,
            }

        case actionTypes.infants_amount:
            return {
                ...state,
                infants: action.payload.infants,
            }

        default:
            return state
    }

}
