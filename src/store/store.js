import {createStore, combineReducers} from 'redux';
import { destinationReducer } from '../reducers/destinationReducer';
import { datesReducer } from '../reducers/datesReducer';
import { passengerReducer } from '../reducers/passengerReducer';
import { urlReducer } from '../reducers/urlReducer';
import { uiReducer } from '../reducers/uiReducer';


const reducers = combineReducers({
    destination: destinationReducer,
    dates: datesReducer,
    passengers: passengerReducer,
    url: urlReducer,
    ui: uiReducer
  });

export const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );