import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './store/store';
import { FlightApp } from './FlightApp';
import './styles/bootstrap-grid.min.css';



ReactDOM.render(
    <Provider store={store}>
      <FlightApp />
    </Provider>,
  
  document.getElementById('root')
);

