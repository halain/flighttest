import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Passenger = ({adults = 0, childrens = 0, infants = 0, amount = 0, handleAdultsChange, handleChildrensChange, handleInfantsChange}) => {

    

    const [StateDropdown, setStateDropdown] = useState({ dropdown: false});

    const { dropdown } = StateDropdown;   
    
    const handleDropDown = () => {
        setStateDropdown({
            dropdown: !dropdown
        })
    }

    return (
        <>

            <div className="passenger-container">

                <div className="passenger" onClick={handleDropDown}>
                    <span> {adults} Adults {childrens} Childrens {infants} Infants </span>
                    <span className="icon-passenger" role="img" aria-label="dropdownd" >🔻</span>
                    <p className="passenger-total"> {amount} Passengers</p>
                </div>

                {dropdown &&

                    <div className="choise-passenger-container">

                        <div className="choise-passenger">
                            <span>Adults</span>
                            <label className="subtitle-passenger-input">12 + years</label>
                            <input
                                type="number"
                                min="0"
                                step="1"
                                name="adult_amount"
                                value={adults}
                                onChange={(e) => handleAdultsChange(e.currentTarget)}
                            />
                        </div>
                        <div className="choise-passenger">
                            <span>Children</span>
                            <label className="subtitle-passenger-input">2-11 years</label>
                            <input
                                type="number"
                                min="0"
                                step="1"
                                name="children_amount"
                                value={childrens}
                                onChange={(e) => handleChildrensChange(e.currentTarget)}
                            />
                        </div>
                        <div className="choise-passenger">
                            <span>Infants</span>
                            <label className="subtitle-passenger-input">0-1 years</label>
                            <input
                                type="number"
                                min="0"
                                step="1"
                                name="infants_amount"
                                value={infants}
                                onChange={(e) => handleInfantsChange(e.currentTarget)}
                            />
                        </div>
                        <div className="choise-passenger">
                            <p className="choise-passenger-close" onClick={handleDropDown}>Close</p>
                        </div>

                    </div>

                }

            </div>

        </>
    )
}

Passenger.propTypes = {
    adults: PropTypes.number.isRequired,
    childrens: PropTypes.number.isRequired,
    infants: PropTypes.number.isRequired,
    amount: PropTypes.number.isRequired,
    handleAdultsChange: PropTypes.func.isRequired, 
    handleChildrensChange: PropTypes.func.isRequired, 
    handleInfantsChange: PropTypes.func.isRequired
}

export default Passenger;
