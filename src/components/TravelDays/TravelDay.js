import React from 'react';
import PropTypes from 'prop-types';

const TravelDay = ({departure_day, return_day, handleDeparture, handleReturn }) => {
    return (
        <ul className="travel-dates">
            <li>
                <input 
                    type="date"
                    name="departure_day"
                    value={departure_day}
                    onChange={(e)=>handleDeparture(e.currentTarget)}
                    />
                </li>
            <li>
                <input 
                    type="date"
                    name="return_day"
                    value={return_day}
                    onChange={(e)=>handleReturn(e.currentTarget)}
                />
            </li>
    </ul>
    )
}

TravelDay.propTypes = {
    departure_day: PropTypes.string.isRequired,
    return_day: PropTypes.string,
    handleDeparture: PropTypes.func.isRequired,
    handleReturn: PropTypes.func.isRequired
}

export default TravelDay;
