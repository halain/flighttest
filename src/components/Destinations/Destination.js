import React from 'react';
import SelectSearch from 'react-select-search';
import PropTypes from 'prop-types';

import { travels } from './data';
import '../../styles/destinations/selectSearch.css';




const Destination = ({origin, destination, handleOrigin, handleDestination }) => {
   

    return (
        <>
        <div className="destination" >
            <SelectSearch
                        options={travels}
                        search
                        placeholder="FROM"
                        value={origin}
                        onChange={handleOrigin}
                    />
            <SelectSearch
                        options={travels}
                        search
                        placeholder="TO"
                        value={destination}
                        onChange={handleDestination}
                    />
        </div>  
        </> 
    )
}

Destination.propTypes = {
    origin: PropTypes.string.isRequired,
    destination: PropTypes.string.isRequired,
    handleOrigin: PropTypes.func.isRequired,
    handleDestination: PropTypes.func.isRequired
}

export default Destination;
