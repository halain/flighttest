export const travels = [
    {value:'BUE', name: 'Buenos Aires'},
    {value:'ADL', name: 'Adelaide'},
    {value:'ATL', name: 'Atlanta'},
    {value:'BCN', name: 'Barcelona'},
    {value:'BRE', name: 'Bremen'},
    {value:'CUN', name: 'Cancun'},
    {value:'MAD', name: 'Madrid'},
  ];

