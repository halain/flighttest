import React from 'react';
import PropTypes from 'prop-types';


const UrlApp = ({url}) => {


    return (
                   
            <div className="url-container">
                <span>
                    {url} 
                </span>
            </div>
       
    )
}

UrlApp.protoTypes = {
    url: PropTypes.string.isRequired
}

export default UrlApp;
