export const actionTypes = {

    origin: '[DESTINATION] Origin',
    destination: '[DESTINATION] Destin',
    departure_day: '[DATES] Departur',
    return_day: '[DATES] Return',
    adult_amount: '[PASSENGERS] Adult amount',
    children_amount: '[PASSENGERS] Children amount',
    infants_amount: '[PASSENGERS] Infant amount',
    url: '[URL]',
    uiSetError: '[UI] Set error',
    uiRemoveError: '[UI] Remove error',

}