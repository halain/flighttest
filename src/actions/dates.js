import { actionTypes } from "../types/actionTypes"



export const setDepartureDay = (departure_day) => {
    return {
        type: actionTypes.departure_day,
        payload: {departure_day}
    }
}


export const setReturnDay = (return_day) => {
    return {
        type: actionTypes.return_day,
        payload: {return_day}
    }
}