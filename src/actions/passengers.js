import { actionTypes } from "../types/actionTypes"



export const setAdultAmount = (adults) => {
    return {
        type: actionTypes.adult_amount,
        payload: {adults}
    }
}


export const setChildrenAmount = (childrens) => {
    return {
        type: actionTypes.children_amount,
        payload: {childrens}
    }
}

export const setInfantsAmount = (infants) => {
    return {
        type: actionTypes.infants_amount,
        payload: {infants}
    }
}


