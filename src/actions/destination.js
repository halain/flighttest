import { actionTypes } from "../types/actionTypes"



export const setOrigin = (origin) => {
    return {
        type: actionTypes.origin,
        payload: {origin}
    }
}


export const setDestination = (destination) => {
    return {
        type: actionTypes.destination,
        payload: {destination}
    }
}