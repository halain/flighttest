import { actionTypes } from '../types/actionTypes';


export const setUrl = (url) => ({
    type: actionTypes.url,
    payload: url   
});


export const removeUrl = () => ({
    type: actionTypes.url
});


