import { actionTypes } from '../types/actionTypes';


export const setError = (err) => ({
    type: actionTypes.uiSetError,
    payload: err    
});


export const removeError = () => ({
    type: actionTypes.uiRemoveError 
});