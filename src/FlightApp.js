import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { setOrigin, setDestination } from './actions/destination';
import { setDepartureDay, setReturnDay } from './actions/dates';
import { setAdultAmount, setChildrenAmount, setInfantsAmount } from './actions/passengers';
import { setError, removeError } from './actions/ui';
import { setUrl, removeUrl } from './actions/url';

import Destination  from './components/Destinations/Destination';
import TravelDay from './components/TravelDays/TravelDay';
import Passenger from './components/Passenger/Passenger';
import UrlApp from './components/url/UrlApp';
import './styles/index.css';


export const FlightApp = () => {

    const dispatch = useDispatch();

    const state = useSelector( state => state );
    const {msgError} =  state.ui;
    const {url} =  state.url;
    const {origin, destination} = state.destination;
    const {departure_day, return_day} = state.dates;
    const { adults, childrens, infants} = state.passengers;

    const handleOrigin = (orig) => dispatch(setOrigin(orig));

    const handleDestination = (dest) => dispatch(setDestination(dest));
    
    const handleDeparture = ({value}) => dispatch(setDepartureDay(value));

    const handleReturn = ({value}) => dispatch(setReturnDay(value));

    const handleAdultsChange = ({value}) => dispatch(setAdultAmount( parseInt(value)));

    const handleChildrensChange = ({value}) => dispatch(setChildrenAmount(parseInt(value)));
    
    const handleInfantsChange = ({value}) => dispatch(setInfantsAmount(parseInt(value)));
        
    const flight_type = () =>  (return_day ==='' ) ? 'Oneway' : 'Outbound'; 

    const passenger_amount = adults + childrens + infants;


    const handleSubmit = (e) => {
        e.preventDefault();
        const url = `https://www.swiss.com/us/en/Book/${flight_type()}/${origin}-${destination}
        /from-${departure_day}/adults-${adults}/children-${childrens}/infants-${infants}/class-economy/al-LX/sidmbvl`;

        if (isFormValid()) {
            dispatch(setUrl(url))
        }else {
            dispatch(removeUrl())
        }
    }

    const isFormValid = () => { 
        let isValidDate = Date.parse(departure_day);
        if (origin.length === 0) { 
            dispatch(setError('Origin is required'))
            return false
        }else if ( destination.length === 0) {
            dispatch(setError('Destination is required'));
            return false;   
        }else if (isNaN(isValidDate)) {
            dispatch(setError('Departure day must be a correct day'));
            return false;
        }else if ( passenger_amount === 0 ){
            dispatch(setError('Passenger is required'));
            return false;
        }
        dispatch(removeError());
        return true;
    }
       
    return (
       
       <>
            <div className="row">                
                <div className="col-md-6 col-sm-12">
                    <h1>Booking Mask</h1>
                </div>
                <div className="col-md-6 col-sm-12">       
                    {   msgError &&                 
                            (   
                                <div className="alert-error">
                                   {msgError}
                                </div>
                            )
                    }
                </div>                 
            </div>
            <hr/>

            <form onSubmit={handleSubmit}>
                <div className="wrapper mt-5">
                    <div className="row">                    
                        <div className="col-md-4 col-sm-12 mt-2">
                        <Destination 
                                origin={origin} 
                                destination={destination} 
                                handleOrigin={handleOrigin} 
                                handleDestination={handleDestination}
                                />
                        </div>
                        <div className="col-md-5 col-sm-12 mt-2">
                            <TravelDay 
                                departure_day={departure_day} 
                                return_day={return_day} 
                                handleDeparture={handleDeparture}  
                                handleReturn={handleReturn}  />
                        </div>
                        <div className="col-lg-2 col-md-4 col-sm-12 mt-2">   
                            <Passenger 
                                adults={adults} 
                                childrens={childrens} 
                                infants={infants} 
                                amount={passenger_amount}
                                handleAdultsChange={handleAdultsChange} 
                                handleChildrensChange={handleChildrensChange} 
                                handleInfantsChange={handleInfantsChange} 
                                />
                            <div className="mt-2">
                            <button className="search" type="submit">
                                    Search
                            </button>
                            </div>
                        </div>
                       
                    </div>                              
                </div>  
            </form>
       

            {  url && <UrlApp url={url}/>  }                  
           
        </>
    )
}
