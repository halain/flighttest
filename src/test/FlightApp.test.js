import React from 'react';
import '@testing-library/jest-dom';
import { mount, shallow } from 'enzyme';
import { act } from '@testing-library/react';
import { FlightApp } from '../FlightApp';
import { Provider } from 'react-redux';
import { store } from '../store/store';
import { travels } from '../components/Destinations/data';




describe('test in <FlightApp />', () => {


    const wrapper = mount(
            <Provider store={store}>
                <FlightApp />
            </Provider>
        )
    

    test('should be displayed correctly', () => {
       
        expect(wrapper).toMatchSnapshot();

    }); 


    test('url should not be displayed', () => {

        //simulate submit form
        wrapper.find('form').prop('onSubmit')( {preventDefault: () => { } });
        expect(wrapper.find('UrlApp').exists()).toBe(false);

    });

    
    
    
})
