import React from 'react';
import '@testing-library/jest-dom';
import Passenger from '../../../components/Passenger/Passenger';
import { shallow } from 'enzyme';




describe('test in <Passenger />', () => {

    const handleAdultsChange = jest.fn();
    const handleChildrensChange = jest.fn();
    const handleInfantsChange = jest.fn();

    const passenger = {
        adults: 0,
        childrens: 0,
        infants: 0,
        amount: 0,
    }
    

    const wrapper = shallow(
        <Passenger 
            adults = {passenger.adults}
            childrens = {passenger.childrens}
            infants = {passenger.infants}
            amount = {passenger.amount}
            handleAdultsChange = {handleAdultsChange}
            handleChildrensChange = {handleChildrensChange}
            handleInfantsChange = {handleInfantsChange}
        />
    );

    test('should be displayed correctly', () => {
        
        expect(wrapper).toMatchSnapshot();

    })
    
});
