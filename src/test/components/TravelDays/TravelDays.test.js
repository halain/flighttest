import React from 'react';
import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import TravelDay from '../../../components/TravelDays/TravelDay';


describe('test in <TravelDasy />', () => {

    const handleDeparture = jest.fn();
    const handleReturn = jest.fn();
    const departure_day = '2020-09-06';
    const return_day = '';

    const wrapper = shallow(
        <TravelDay 
            departure_day={departure_day} 
            return_day={return_day} 
            handleDeparture={handleDeparture}
            handleReturn={handleReturn}
            />
    );

    test('should be displayed correctly', () => {
        
        expect(wrapper).toMatchSnapshot();

    });
    
    
})
