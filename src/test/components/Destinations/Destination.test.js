import React from 'react';
import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import Destination from '../../../components/Destinations/Destination';
import { travels } from '../../../components/Destinations/data';



describe('test in  en  <Destination />', () => {
   
    const handleOrigin = jest.fn();
    const handleDestination = jest.fn();


    const wrapper = shallow(
                <Destination 
                    origin = {travels[0].name}
                    destination = {travels[1].name}
                    handleOrigin = {handleOrigin}
                    handleDestination = {handleDestination}
                />
                )


    test('should be displayed correctly', () => {
       
        expect(wrapper).toMatchSnapshot();

    });   

});
