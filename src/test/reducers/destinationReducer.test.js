import '@testing-library/jest-dom';
import { destinationReducer } from '../../reducers/destinationReducer';
import { actionTypes } from '../../types/actionTypes';
import { travels } from '../../components/Destinations/data';


describe('Test in destinationReducer', () => {

    const initialState = {
        origin: '',
        destination: ''
    }

    test('should return default state', () => {
        
        
        const state = destinationReducer(initialState,{});

        expect(state).toEqual(initialState);
    });


    test('should return origin', () => {
        
        const origin = travels[0].name;

        const action = {
            type: actionTypes.origin,
            payload: {origin}
         };
        
        const state = destinationReducer(initialState, action);
        
        expect(state).toEqual({...initialState,origin})
        expect(state.origin).toBe(origin);
    });

    test('should return destination', () => {
        
        const destination = travels[3].name;

        const initialState = {
            origin: 'Adelaide',
            destination: ''
        }

        const action = {
            type: actionTypes.destination,
            payload: {destination}
         };

        
        const state = destinationReducer(initialState,action);

        expect(state).toEqual({...initialState, destination});
        expect(state.destination).toBe(destination);
    });


    

});