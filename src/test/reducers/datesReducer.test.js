import '@testing-library/jest-dom';
import { datesReducer } from '../../reducers/datesReducer';
import { actionTypes } from '../../types/actionTypes';



describe('Test in datesReducer', () => {

    const initialState = {
        departure_day: '',
        return_day: ''
    }
    

    test('should return default state', () => {
        
        
        const state = datesReducer(initialState,{});

        expect(state).toEqual(initialState);
    });


    test('should return departure_day', () => {
        
        const departure_day = '2020-09-06';

        const action = {
            type: actionTypes.departure_day,
            payload: {departure_day}
         };
        
        const state = datesReducer(initialState, action);
        
        expect(state).toEqual({...initialState,departure_day})
        expect(state.departure_day).toBe(departure_day);
    });

    test('should return return_day', () => {
        
        const return_day = '2020-09-15';

        const initialState = {
            departure_day: '2020-09-06',
            return_day: ''
        }

        const action = {
            type: actionTypes.return_day,
            payload: {return_day}
         };

        
        const state = datesReducer(initialState,action);

        expect(state).toEqual({...initialState, return_day});
        expect(state.return_day).toBe(return_day);
    });


    

});