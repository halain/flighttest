import '@testing-library/jest-dom';
import { urlReducer } from '../../reducers/urlReducer';
import { actionTypes } from '../../types/actionTypes';



describe('Test in urlReducer', () => {

    const initialState ={
        url:''
    }
    
    test('should return default state', () => {
        
        const state = urlReducer(initialState,{});

        expect(state).toEqual(initialState);
    });


    test('should return url', () => {
        
        const url = 'https://google.com';

        const action = {
            type: actionTypes.url,
            payload: url
         };
        
        const state = urlReducer(initialState, action);
        
        expect(state).toEqual({...initialState,url})
        expect(state.url).toBe(url);
    });


});