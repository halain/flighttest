import '@testing-library/jest-dom';
import { uiReducer } from '../../reducers/uiReducer';
import { actionTypes } from '../../types/actionTypes';



describe('Test in uiReducer', () => {

    const initialState = {
        msgError: null
    }
    
    
    test('should return default state', () => {
        
        const state = uiReducer(initialState,{});

        expect(state).toEqual(initialState);
    });


    test('should return error message', () => {
        
        const msgError = 'Departure day is required';

        const action = {
            type: actionTypes.uiSetError,
            payload: msgError
         };
        
        const state = uiReducer(initialState, action);
        
        expect(state).toEqual({...initialState,msgError});
        expect(state.msgError).toBe(msgError);
       
    });


    test('should remove error message', () => {
        
        const initialState = {
            msgError: 'Departure day is required'
        }

        const action = {
            type: actionTypes.uiRemoveError
         };
        
        const state = uiReducer(initialState, action);
        
        expect(state.msgError).toBe(null);
       
    });



});