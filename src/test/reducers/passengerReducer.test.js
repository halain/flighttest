import '@testing-library/jest-dom';
import { passengerReducer } from '../../reducers/passengerReducer';
import { actionTypes } from '../../types/actionTypes';



describe('Test in passengerReducer', () => {

    const initialState = {
        adults: 1, 
        infants: 0,
        childrens: 0
    }
    
    
    test('should return default state', () => {
        
        const state = passengerReducer(initialState,{});

        expect(state).toEqual(initialState);
    });


    test('should return adults amount', () => {
        
        const adults = 5;

        const action = {
            type: actionTypes.adult_amount,
            payload: {adults}
         };
        
        const state = passengerReducer(initialState, action);
        
        expect(state).toEqual({...initialState,adults})
        expect(state.adults).toBe(adults);
    });


    test('should return childens amount', () => {
        
        const childrens = 2;

        const action = {
            type: actionTypes.children_amount,
            payload: {childrens}
         };
       
        const state = passengerReducer(initialState,action);

        expect(state).toEqual({...initialState, childrens});
        expect(state.childrens).toBe(childrens);
    });


    test('should return infants amount', () => {
        
        const infants = 1;

        const action = {
            type: actionTypes.infants_amount,
            payload: {infants}
         };
       
        const state = passengerReducer(initialState,action);

        expect(state).toEqual({...initialState, infants});
        expect(state.infants).toBe(infants);
    });


});